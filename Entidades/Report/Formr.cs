﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Report
{
    public partial class Formr : Form
    {
        string parametro = "";
        public Formr(string parametro = "")
        {
            this.parametro = parametro;
            InitializeComponent();
        }

        private void Formr_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dtEstadoPagoPostulantes.DataTable1' Puede moverla o quitarla según sea necesario.
            this.DataTable1TableAdapter.Fill(this.dtEstadoPagoPostulantes.DataTable1, parametro);

            this.reportViewer1.RefreshReport();
        }
    }
}
