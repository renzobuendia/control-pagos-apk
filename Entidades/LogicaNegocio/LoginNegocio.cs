﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;

namespace LogicaNegocio
{
    public class LoginNegocio
    {

        LoginDatos login = new LoginDatos();

        public DataTable Login(Entidades.Usuario usuario)
        {

            return login.Login_DB(usuario);

        }

        public DataTable ComprobarUsuario(Entidades.Usuario usuario)
        {

            return login.ComprobarUsuario(usuario);

        }

        public DataTable DevolverListaParticipantes()
        {

            return login.DevolverListaParticipantes();

        }
    }
}
