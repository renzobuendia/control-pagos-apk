﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{

    public class CuotasNegocio
    {
        CuotasDatos cuotasDatos = new CuotasDatos();
        public bool Insertar(Programa programa)
        {

            return cuotasDatos.Insertar(programa);

        }

        public DataTable ObtenerId()
        {

            return cuotasDatos.ObtenerId();

        }

        public DataTable DetallePrograma(int idPrograma)
        {

            return cuotasDatos.DetallePrograma(idPrograma);

        }
        public DataTable BuscarCuotas(int idPrograma)
        {

            return cuotasDatos.BuscarCuotas(idPrograma);

        }
        public bool Insertar(string idPrograma, DateTime FechaLimite, decimal monto, string NumeroCuota)
        {

            return cuotasDatos.Insertar(idPrograma, FechaLimite,monto,NumeroCuota);

        }
        public bool Modificar(string idPrograma, DateTime FechaLimite, decimal monto, string NumeroCuota)
        {

            return cuotasDatos.Modificar(idPrograma, FechaLimite, monto, NumeroCuota);

        }
    }
    
}
