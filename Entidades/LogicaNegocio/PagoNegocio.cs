﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class PagoNegocio
    {
        PagoDatos pagoDatos = new PagoDatos();
        public DataTable Buscar(string idParticipante)
        {
            return pagoDatos.Buscar(idParticipante);
        }
        public DataTable BuscarEstadoPago(string idParticipante)
        {
            return pagoDatos.BuscarEstadoPago(idParticipante);
        }
        public DataTable BuscarDatosPago(string idPrograma, string NumeroCuota)
        {
            return pagoDatos.BuscarDatosPago( idPrograma,  NumeroCuota);
        }
        public bool Insertar(Pago pago, DateTime fechaRegistro)
        {

            return pagoDatos.Insertar(pago, fechaRegistro);

        }
        public bool ActualizarEstadopago(string idPostulante, string EstadoPago)
        {

            return pagoDatos.ActualizarEstadopago( idPostulante,  EstadoPago);

        }

        public bool Eliminar(string voucher)
        {

            return pagoDatos.Eliminar(voucher);

        }
        public bool ModificarRecomendados(decimal MontoCuota, string idPostulante)
        {

            return pagoDatos.ModificarRecomendados(MontoCuota, idPostulante);

        }
        public bool EliminarRecomendados(string idPostulante)
        {

            return pagoDatos.EliminarRecomendados(idPostulante);

        }
    }
}
