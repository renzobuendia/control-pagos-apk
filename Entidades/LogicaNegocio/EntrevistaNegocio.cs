﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{

    public class EntrevistaNegocio
    {

        EntrevistaDatos entrevistaDatos = new EntrevistaDatos();
        public DataTable Buscar(string idPostulante)
        {

            return entrevistaDatos.Buscar(idPostulante);

        }

        public DataTable BuscarPrograma()
        {

            return entrevistaDatos.BuscarPrograma();

        }

        public bool Modificar(Entrevista entrevista, string idPrograma)
        {

            return entrevistaDatos.Modificar(entrevista, idPrograma);


        }
        public bool Insertar(Entrevista entrevista, string idPrograma, string EstadoPago)
        {

            return entrevistaDatos.Insertar(entrevista, idPrograma, EstadoPago);

        }
        public DataTable VerificarVoucher(string Voucher)
        {

            return entrevistaDatos.VerificarVoucher(Voucher);

        }
    }
}
