﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class ParticipanteNegocio
    {

        ParticipanteDatos participanteDatos = new ParticipanteDatos();

        public string Guardar (Entidades.Participante participante)
        {
            return participanteDatos.Guardar(participante);
        }
        public DataTable Modificar(Entidades.Participante participante)
        {
            return participanteDatos.Modificar(participante);
        }
        public DataTable Eliminar(Entidades.Participante participante)
        {
            return participanteDatos.Eliminar(participante);
        }
        public DataTable Buscar(string parametro, string valor)
        {
            return participanteDatos.Buscar(parametro,valor);
        }

        public string ValidaExistente(Entidades.Participante participante)
        {
            return participanteDatos.ValidaExistente(participante);
        }
    }
}
