﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using Entidades;


namespace AccesoDatos
{
    public class LoginDatos
    {

        public DataTable Login_DB(Usuario usuario)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("consultaUsuario", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@usuario", usuario.usuario);
            cmd.Parameters.AddWithValue("@pass", usuario.password);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }

        public DataTable ComprobarUsuario(Usuario usuario)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("consultaNomUsuario", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@usuario", usuario.usuario);
            
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }
        public DataTable DevolverListaParticipantes()
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarTodosParticipante", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }
    }
}
