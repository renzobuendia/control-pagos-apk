﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class ParticipanteDatos
    {

        public string Guardar(Participante participante)
        {
            string id;
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_GuardarParticipante", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", participante.nombre);
            cmd.Parameters.AddWithValue("@apellido", participante.apellido);
            cmd.Parameters.AddWithValue("@correo", participante.correo);
            cmd.Parameters.AddWithValue("@celular", participante.celular);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            id = tabla.Rows[0][0].ToString();
            return id;
        }
        public DataTable Modificar(Participante participante)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_ModificarParticipante", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", participante.nombre);
            cmd.Parameters.AddWithValue("@apellido", participante.apellido);
            cmd.Parameters.AddWithValue("@correo", participante.correo);
            cmd.Parameters.AddWithValue("@celular", participante.celular);
            cmd.Parameters.AddWithValue("@id", participante.id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }
        public DataTable Eliminar(Participante participante)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_EliminarParticipante", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", participante.id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }
        public DataTable Buscar(string parametro, string valor)
        {

            if (parametro == "Nombres")
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_BuscarParticipante", conectar.cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombre", valor);
                cmd.Parameters.AddWithValue("@apellido", "");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable tabla = new DataTable();
                da.Fill(tabla);
                return tabla;
            }
            else
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_BuscarParticipante", conectar.cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombre", "");
                cmd.Parameters.AddWithValue("@apellido", valor);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable tabla = new DataTable();
                da.Fill(tabla);
                return tabla;
            }

        }
        public string ValidaExistente(Participante participante)
        {
            string mensaje;
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_VerificarPostulanteDuplicado", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", participante.nombre);
            cmd.Parameters.AddWithValue("@apellido", participante.apellido);
            cmd.Parameters.AddWithValue("@correo", participante.correo);
            cmd.Parameters.AddWithValue("@celular", participante.celular);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            mensaje = tabla.Rows[0][0].ToString();
            return mensaje;
        }
    }
}
