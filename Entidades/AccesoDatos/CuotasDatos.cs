﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class CuotasDatos
    {

        public bool Insertar(Programa programa)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_AgregarPrograma", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", programa.id);
                cmd.Parameters.AddWithValue("@Tipo", programa.nombre);
                cmd.Parameters.AddWithValue("@Costo", programa.costo);
                cmd.Parameters.AddWithValue("@Periodo", programa.periodo);
                cmd.Parameters.AddWithValue("@TipoMoneda", programa.tipoMoneda);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
        public DataTable ObtenerId()
        {

            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_ObtenerId", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;

        }
        public DataTable DetallePrograma(int idPrograma)
        {
            
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarDetallesPrograma", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;

        }
        public DataTable BuscarCuotas(int idPrograma)
        {

            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarCuotas", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;

        }
        public bool Insertar(string idPrograma, DateTime FechaLimite, decimal monto, string NumeroCuota)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_InsertarCuotas", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
                cmd.Parameters.AddWithValue("@FechaLimite", FechaLimite);
                cmd.Parameters.AddWithValue("@monto", monto);
                cmd.Parameters.AddWithValue("@NumeroCuota", NumeroCuota);
          

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
        public bool Modificar(string idPrograma, DateTime FechaLimite, decimal monto, string NumeroCuota)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_ModificarCuotas", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
                cmd.Parameters.AddWithValue("@FechaLimite", FechaLimite);
                cmd.Parameters.AddWithValue("@monto", monto);
                cmd.Parameters.AddWithValue("@NumeroCuota", NumeroCuota);


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
    }
}
