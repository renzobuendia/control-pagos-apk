﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class EntrevistaDatos
    {

        public DataTable Buscar(string idPostulante)
        {

            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarEntrevista", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPostulante", idPostulante);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;

        }
        public DataTable BuscarPrograma()
        {

            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarPrograma", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;

        }

        public bool Modificar(Entrevista entrevista, string idPrograma)
        {
            try
            {
                Conexion conectar = new Conexion();

                SqlCommand cmd = new SqlCommand("sp_ModificarEntrevista", conectar.cn);
                conectar.cn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@encargado", entrevista.encargado);
                cmd.Parameters.AddWithValue("@monto", entrevista.montoEntrevista);
                cmd.Parameters.AddWithValue("@descripcion", entrevista.descripcion);
                cmd.Parameters.AddWithValue("@fechaEntrevista", entrevista.fechaEntrevista);
                cmd.Parameters.AddWithValue("@fechaPago", entrevista.fechaPago);
                cmd.Parameters.AddWithValue("@operacion", entrevista.numeroOperacion);
                cmd.Parameters.AddWithValue("@idPostulante", entrevista.idPostulante);
                cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            
        }
        public bool Insertar(Entrevista entrevista, string idPrograma, string EstadoPago)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_InsertarEntrevista", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@encargado", entrevista.encargado);
                cmd.Parameters.AddWithValue("@monto", entrevista.montoEntrevista);
                cmd.Parameters.AddWithValue("@descripcion", entrevista.descripcion);
                cmd.Parameters.AddWithValue("@fechaEntrevista", entrevista.fechaEntrevista);
                cmd.Parameters.AddWithValue("@fechaPago", entrevista.fechaPago);
                cmd.Parameters.AddWithValue("@operacion", entrevista.numeroOperacion);
                cmd.Parameters.AddWithValue("@idPostulante", entrevista.idPostulante);
                cmd.Parameters.AddWithValue("@EstadoPago", EstadoPago);
                cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
        public DataTable VerificarVoucher(string Voucher)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarVoucher", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@NumeroVoucher", Voucher);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }

    }
}
