﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class PagoDatos
    {

        public DataTable Buscar (string idParticipante )
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarPagos", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPostulante", idParticipante);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }

        public DataTable BuscarEstadoPago(string idParticipante)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarEstadoPagoPostulante", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPostulante", idParticipante);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }

        public DataTable BuscarDatosPago(string idPrograma, string NumeroCuota)
        {
            Conexion conectar = new Conexion();
            SqlCommand cmd = new SqlCommand("sp_BuscarDatosPagos", conectar.cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idPrograma", idPrograma);
            cmd.Parameters.AddWithValue("@NumeroCuota", NumeroCuota);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tabla = new DataTable();
            da.Fill(tabla);
            return tabla;
        }
        public bool Insertar(Pago pago, DateTime fechaRegistro)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_InsertarPago", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idPostulante", pago.idPostulante);
                cmd.Parameters.AddWithValue("@NumeroCuota", pago.numeroCuota);
                if(pago.numeroCuota== "Descuento por Recomendación")
                {
                    cmd.Parameters.AddWithValue("@NumeroVoucher", "");
                }
                else
                {
                cmd.Parameters.AddWithValue("@NumeroVoucher", pago.numeroVoucher);
                }
                cmd.Parameters.AddWithValue("@Fecha", pago.fechaPago);
                cmd.Parameters.AddWithValue("@Monto", pago.montoPago);
                cmd.Parameters.AddWithValue("@IdUsuario", pago.Encargado);
                cmd.Parameters.AddWithValue("@FechaRegistro", fechaRegistro);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
        public bool ActualizarEstadopago(string idPostulante, string EstadoPago)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_ActualizarEstadoPago", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idPostulante", idPostulante);
                cmd.Parameters.AddWithValue("@EstadoPago", EstadoPago);


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
        public bool Eliminar(string voucher)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_EliminarPago", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@voucher", voucher);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

        public bool ModificarRecomendados(decimal MontoCuota, string idPostulante)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_ModificarRecomendados", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MontoCuota", MontoCuota);
                cmd.Parameters.AddWithValue("@idPostulante", Convert.ToInt32(idPostulante));

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
        public bool EliminarRecomendados(string idPostulante)
        {
            try
            {
                Conexion conectar = new Conexion();
                SqlCommand cmd = new SqlCommand("sp_EliminarRecomendados", conectar.cn);
                conectar.cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idPostulante", Convert.ToInt32(idPostulante));

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                int filasAfectadas = cmd.ExecuteNonQuery();
                conectar.cn.Close();
                if (filasAfectadas != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }
    }
}
