﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Participante
    {

        public string nombre { get; set; }
        public string apellido { get; set; }
        public string id { get; set; }
        public string celular { get; set; }
        public string correo { get; set; }

    }
}
