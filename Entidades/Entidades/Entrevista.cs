﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Entrevista
    {

        public string idPostulante { get; set; }
        public string encargado { get; set; }
        public decimal montoEntrevista { get; set; }
        public string descripcion { get; set; }
        public DateTime fechaEntrevista { get; set; }
        public string numeroOperacion { get; set; }
        public DateTime fechaPago { get; set; }

    }
}
