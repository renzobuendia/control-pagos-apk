﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Pago
    {

        public string idPostulante { get; set; }

        public string numeroCuota { get; set; }

        public string numeroVoucher { get; set; }

        public DateTime fechaPago { get; set; }

        public decimal montoPago { get; set; }

        public string Encargado { get; set; }
    }
}
