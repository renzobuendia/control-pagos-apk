﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Programa
    {

        public string id { get; set; }
        public decimal costo { get; set; }
        public string nombre { get; set; }

        public string periodo { get; set; }
        public string tipoMoneda { get; set; }
    }
}
