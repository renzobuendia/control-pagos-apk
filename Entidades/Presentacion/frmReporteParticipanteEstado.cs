﻿using Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmReporteParticipanteEstado : Form
    {
        
        public frmReporteParticipanteEstado()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form frmReporteEstadoPago = new Formr(cmbEstadoPago.Text);
            //frmEntrevista.MdiParent = this;
            frmReporteEstadoPago.Show();
        }

        private void frmReporteParticipanteEstado_Load(object sender, EventArgs e)
        {
            cmbEstadoPago.SelectedIndex = 0;
        }
    }
}
