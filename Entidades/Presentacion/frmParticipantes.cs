﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmParticipantes : Form
    {
      
        LogicaNegocio.ParticipanteNegocio participanteNegocio = new LogicaNegocio.ParticipanteNegocio();
        public frmParticipantes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtNombres.Text=="" || txtApellidos.Text=="" || txtCorreo.Text=="" || txtCelular.Text=="")
            {
                MessageBox.Show("Falta rellenar algunos campos");
            }
            else
            {
                try
                {
                    Entidades.Participante participante = new Entidades.Participante();
                    participante.apellido = Regex.Replace(txtApellidos.Text.Trim(), @"(\s)\s+", " ");
                    participante.nombre = Regex.Replace(txtNombres.Text.Trim(), @"(\s)\s+", " ");
                    participante.correo = txtCorreo.Text;
                    participante.celular = txtCelular.Text;


                    if (txtId.Text == "")
                    {
                        string mensajeDuplicado = participanteNegocio.ValidaExistente(participante);
                        if (mensajeDuplicado != "")
                        {
                            MessageBox.Show(mensajeDuplicado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        participante.id = participanteNegocio.Guardar(participante);
                        txtId.Text = participante.id;
                        MessageBox.Show("Participante Guardado");
                    }
                    else

                    {
                        participante.id = txtId.Text;
                        participanteNegocio.Modificar(participante);
                        txtId.Text = participante.id;
                        MessageBox.Show("Participante Modificado");
                    }

                }
                catch
                {
                    MessageBox.Show("Ocurrio un error al intentar guardar el postulante nuevo");
                }
            }
         
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

            HabilitaCampos();
            ActiveControl = txtNombres;

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmParticipantes_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
            
        }
        private void txtCorreo_Validating(object sender, CancelEventArgs e)
        {
            System.Text.RegularExpressions.Regex rEMail = new System.Text.RegularExpressions.Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,4})+)$");

            if (txtCorreo.Text.Length > 0)

            {

                if (!rEMail.IsMatch(txtCorreo.Text))

                {

                    MessageBox.Show("El email no es válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    txtCorreo.SelectAll();

                    e.Cancel = true;

                }

            }
        }

        private void txtApellidos_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (Char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtNombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void frmParticipantes_Load(object sender, EventArgs e)
        {
            txtApellidos.CharacterCasing = CharacterCasing.Upper;
            txtNombres.CharacterCasing = CharacterCasing.Upper;
            txtCorreo.CharacterCasing = CharacterCasing.Lower;
            ActiveControl = txtNombres;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(txtId.Text=="")
            {
                MessageBox.Show("No hay participante por eliminar");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar al participante?", "ADVERTENCIA", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Entidades.Participante participante = new Entidades.Participante();
                participante.id = txtId.Text;
                participanteNegocio.Eliminar(participante);
                txtId.Text = participante.id;
                MessageBox.Show("Participante Eliminado");
                HabilitaCampos();
            }
          
            
        }

        public void HabilitaCampos()
        {
            txtId.Text = "";
            txtApellidos.Text = "";
            txtNombres.Text = "";
            txtCorreo.Text = "";
            txtCelular.Text = "";
            txtApellidos.ReadOnly = false;
            txtNombres.ReadOnly = false;
            txtCelular.ReadOnly = false;
            txtCorreo.ReadOnly = false;
        }

        private void btnBuscar2_Click(object sender, EventArgs e)
        {
            using (var form = new frmBuscar())
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Entidades.Participante participanteBusqueda = new Entidades.Participante();            //values preserved after close
                    participanteBusqueda = form.participante;

                    txtId.Text = participanteBusqueda.id;
                    txtApellidos.Text = participanteBusqueda.apellido;
                    txtNombres.Text = participanteBusqueda.nombre;
                    txtCorreo.Text = participanteBusqueda.correo;
                    txtCelular.Text = participanteBusqueda.celular;
                    txtApellidos.ReadOnly = false;
                    txtNombres.ReadOnly = false;
                    txtCelular.ReadOnly = false;
                    txtCorreo.ReadOnly = false;
                }
            }
        }

        private void txtCelular_Validating(object sender, CancelEventArgs e)
        {
            if (txtCelular.ReadOnly == false)
            { 
                if (txtCelular.TextLength < 9  )
            {
                MessageBox.Show("Número de celular incorrecto");
                ActiveControl = txtCelular;
                return;
            }
            if (txtCelular.Text.Substring(0, 1) != "9" )
            {
                MessageBox.Show("Número de celular incorrecto");
                ActiveControl = txtCelular;
                return;

            }  
            }
        }

        private void txtCorreo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }

        private void txtNombres_KeyDown(object sender, KeyEventArgs e)
        {


            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    btnBuscar.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtApellidos_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtNombres.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtCorreo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtApellidos.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtCorreo.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
            if (e.KeyCode == Keys.V && e.Modifiers == Keys.Control)
            {
                //Valida copy and paste com caracteres especiais
                txtCelular.Text = Clipboard.GetText().Replace(" ","");
            }
        }
    }
}
