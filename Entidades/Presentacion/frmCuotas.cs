﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmCuotas : Form
    {
        LogicaNegocio.CuotasNegocio cuotasNegocio = new LogicaNegocio.CuotasNegocio();
        EntrevistaNegocio entrevistaNegocio = new EntrevistaNegocio();
        PagoNegocio pagoNegocio = new PagoNegocio();
        string estado;
        public frmCuotas()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbTipo.SelectedIndex >= 0 && txtCosto.Text != "" && txtPeriodo.Text != "" && cbTipoMoneda.SelectedIndex >= 0)
            {
                Entidades.Programa programa = new Entidades.Programa();
                DataTable IdPrograma = new DataTable();
                IdPrograma = cuotasNegocio.ObtenerId();
                programa.id = IdPrograma.Rows[0][0].ToString();
                programa.costo = Convert.ToDecimal(txtCosto.Text);
                programa.nombre = cbTipo.Text;
                programa.periodo = txtPeriodo.Text;
                programa.tipoMoneda = cbTipoMoneda.Text;


                bool correcto;
                correcto = cuotasNegocio.Insertar(programa);
                if (correcto)
                {
                    MessageBox.Show("Se agregó correctamente el nuevo Programa", "CORRECTO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cboPrograma.DataSource = null;
                    cboPrograma.DataSource = entrevistaNegocio.BuscarPrograma();
                    cboPrograma.DisplayMember = "Nombre";
                    cboPrograma.ValueMember = "ID";
                }
                else
                {
                    MessageBox.Show("Ocurrió un error al grabar los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Tiene que completar los campos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private void txtCosto_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void txtPeriodo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space || char.IsPunctuation(e.KeyChar));
        }

        private void frmCuotas_Load(object sender, EventArgs e)
        {
            cboPrograma.DataSource = entrevistaNegocio.BuscarPrograma();
            cboPrograma.DisplayMember = "Nombre";
            cboPrograma.ValueMember = "ID";
        }

        private void cboPrograma_SelectedIndexChanged(object sender, EventArgs e)
        {



        }

        private void cboPrograma_ValueMemberChanged(object sender, EventArgs e)
        {

        }

        private void cboPrograma_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = cuotasNegocio.DetallePrograma(Convert.ToInt32(cboPrograma.SelectedValue.ToString()));
                txtMontoPrograma.Text = dt.Rows[0][1].ToString();
                txtMoneda.Text = dt.Rows[0][0].ToString();

            }
            catch
            {
                DataTable dt = new DataTable();
                dt = cuotasNegocio.DetallePrograma(1);
                txtMontoPrograma.Text = dt.Rows[0][1].ToString();
                txtMoneda.Text = dt.Rows[0][0].ToString();

            }
            try
            {

                DataTable dtCuotas = new DataTable();
                dtCuotas = cuotasNegocio.BuscarCuotas(Convert.ToInt32(cboPrograma.SelectedValue.ToString()));
                dtPrimero.Value = Convert.ToDateTime(dtCuotas.Rows[0][2].ToString());
                txtMontoPrimera.Text = dtCuotas.Rows[0][1].ToString();
                dtSegundo.Value = Convert.ToDateTime(dtCuotas.Rows[1][2].ToString());
                txtMontoSegunda.Text = dtCuotas.Rows[1][1].ToString();
                dtTercero.Value = Convert.ToDateTime(dtCuotas.Rows[2][2].ToString());
                txtMontoTercera.Text = dtCuotas.Rows[2][1].ToString();
                estado = "Modificar";
            }
            catch
            {


                dtPrimero.Value = DateTime.Today;
                txtMontoPrimera.Text = "";
                dtSegundo.Value = DateTime.Today.AddDays(1);
                txtMontoSegunda.Text = "";
                dtTercero.Value = DateTime.Today.AddDays(2);
                txtMontoTercera.Text = "";
                estado = "Insertar";
            }
        }

        private void dtPrimero_ValueChanged(object sender, EventArgs e)
        {
            dtSegundo.MinDate = dtPrimero.Value.AddDays(1);
        }

        private void dtSegundo_ValueChanged(object sender, EventArgs e)
        {
            dtTercero.MinDate = dtSegundo.Value.AddDays(1);
        }

        private void txtMontoPrimera_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void txtMontoSegunda_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void txtMontoTercera_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if ((Convert.ToDecimal(txtMontoPrimera.Text) + Convert.ToDecimal(txtMontoSegunda.Text) + Convert.ToDecimal(txtMontoTercera.Text)) == Convert.ToDecimal(txtMontoPrograma.Text))
            {
                bool correcto = true;
                if (estado == "Insertar")
                {
                    correcto = cuotasNegocio.Insertar(cboPrograma.SelectedValue.ToString(), dtPrimero.Value, Convert.ToDecimal(txtMontoPrimera.Text), "Primera");
                    correcto = cuotasNegocio.Insertar(cboPrograma.SelectedValue.ToString(), dtSegundo.Value, Convert.ToDecimal(txtMontoSegunda.Text), "Segunda");
                    correcto = cuotasNegocio.Insertar(cboPrograma.SelectedValue.ToString(), dtTercero.Value, Convert.ToDecimal(txtMontoTercera.Text), "Tercera");
                }
                else
                {
                    correcto = cuotasNegocio.Modificar(cboPrograma.SelectedValue.ToString(), dtPrimero.Value, Convert.ToDecimal(txtMontoPrimera.Text), "Primera");
                    correcto = cuotasNegocio.Modificar(cboPrograma.SelectedValue.ToString(), dtSegundo.Value, Convert.ToDecimal(txtMontoSegunda.Text), "Segunda");
                    correcto = cuotasNegocio.Modificar(cboPrograma.SelectedValue.ToString(), dtTercero.Value, Convert.ToDecimal(txtMontoTercera.Text), "Tercera");
                }
                if (correcto)
                {
                    MessageBox.Show("Se guardó correctamente", "CORRECTO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("No se puede guardar los datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("La suma de las cuotas no coincide con el monto total", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            bool correcto = true;

            correcto = cuotasNegocio.Modificar("1", dtFechaComportamiento.Value, 160, "Comportamiento-Charla-Vuelo y Sevis");
            if (correcto)
            {

               MessageBox.Show("Se guardó correctamente la fecha", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                correcto = cuotasNegocio.Insertar("1", dtFechaComportamiento.Value, 160, "Comportamiento-Charla-Vuelo y Sevis");
                if (correcto)
                {
                    MessageBox.Show("Se guardó correctamente la fecha", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("No se puede guardar los datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }
    }
}
