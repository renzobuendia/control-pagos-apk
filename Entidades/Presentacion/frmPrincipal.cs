﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace Presentacion
{
    public partial class frmPrincipal : Form
    {

        public string usuario ;
        public string idUsuario ;
        MdiClient ctlMDI;

        public frmPrincipal()

        {
           
            InitializeComponent();
           
        }


        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            
            this.IsMdiContainer = true;
            
            foreach (Control ctl in this.Controls)
            {
                try
                {

                    ctlMDI = (MdiClient)ctl;

                    ctlMDI.BackgroundImage = Properties.Resources.formgran2;
                    
                }
                catch
                {

                }
              
            }



        }

        private void MenuEntrevista_Click(object sender, EventArgs e)
        {
            
                Form frmEntrevista = new frmEntrevista(idUsuario, usuario);
                //frmEntrevista.MdiParent = this;
                frmEntrevista.ShowDialog();
                
      

        }

        private void MenuPostulantes_Click(object sender, EventArgs e)
        {
            
                Form frmParticipantes = new frmParticipantes();
            //frmParticipantes.MdiParent = this;
            frmParticipantes.ShowDialog();
            
       
        }
        private void MenuCuotas_Click(object sender, EventArgs e)
        {
           
            Form frmCuotas = new frmRegistrarPagos(idUsuario, usuario);
            //frmCuotas.MdiParent = this;
            frmCuotas.ShowDialog();
            

        }

        private void cUOTASToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Form frmCuotas = new frmCuotas();
            //frmCuotas.MdiParent = this;
            frmCuotas.ShowDialog();
           
        }

        private void rEPORTEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmReporte = new frmReporteParticipanteEstado();
            //frmCuotas.MdiParent = this;
            frmReporte.ShowDialog();
        }
    }
}
