﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmRegistrarPagos : Form
    {
        PagoNegocio pagoNegocio = new PagoNegocio();
        EntrevistaNegocio entrevistaNegocio = new EntrevistaNegocio();
        Pago pago = new Pago();
        string idParticipante = "";
        string idPrograma = "";
        string idEncargado;
        string encargado;
        decimal montoCuota;
        public frmRegistrarPagos(string idEncargado, string encargado)
        {
            InitializeComponent();
            this.idEncargado = idEncargado;
            this.encargado = encargado;
        }

        private void frmCuota_Load(object sender, EventArgs e)
        {
            dtpFechaPago.MaxDate = DateTime.Today;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            using (var form = new frmBuscar())
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    dgvPagos.DataSource = null;
                    txtEstadoPago.Text = "";
                    txtMontoPrograma.Text = "";
                    txtMoneda.Text = "";
                    txtDeudaPend.Text = "0";
                    Entidades.Participante participanteBusqueda = new Entidades.Participante();            //values preserved after close
                    participanteBusqueda = form.participante;

                    idParticipante = participanteBusqueda.id;
                    DataTable dt2 = new DataTable();
                    dt2 = entrevistaNegocio.Buscar(idParticipante);
                    //Validación si ya tiene entrevista
                    if (dt2.Rows.Count > 0)
                    {

                        txtNombre.Text = participanteBusqueda.nombre + " " + participanteBusqueda.apellido;
                        DataTable dt = new DataTable();

                        dgvPagos.DataSource = pagoNegocio.Buscar(idParticipante);
                        dt = pagoNegocio.BuscarEstadoPago(idParticipante);
                        txtEstadoPago.Text = dt.Rows[0][0].ToString();
                        txtMontoPrograma.Text = dt.Rows[0][2].ToString();
                        txtMoneda.Text = dt.Rows[0][4].ToString();
                        idPrograma = dt.Rows[0][1].ToString();

                        ActualizarDatosPago();
                        LimpiarCampos();
                        decimal sumaDeuda = 0;
                        txtDeudaPend.Text = txtMontoPrograma.Text;
                        for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                        {
                            if (dgvPagos.Rows[i].Cells[0].Value.ToString() != "Comportamiento-Charla-Vuelo y Sevis")
                            {
                                sumaDeuda += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                                txtDeudaPend.Text = (Convert.ToDecimal(txtMontoPrograma.Text) - sumaDeuda).ToString();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("El participante no tiene Entrevista", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            bool RegistroDescuento = false;
            for (int i = 0; i < dgvPagos.Rows.Count; ++i)
            {
                if (dgvPagos.Rows[i].Cells[0].Value.ToString() == "Descuento por Recomendación")
                {
                    RegistroDescuento = true;
                }
            }

            if ((txtEstadoPago.Text == "Falta cancelar Tercera cuota" || txtEstadoPago.Text == "Falta cancelar Comportamiento-Charla-Vuelo y Sevis"))
            {
                txtCantidadRecomendado.Visible = true;
                lblCantidadRecomendado.Visible = true;
                lblDescuentoTotal.Visible = true;
                txtDescuento.Visible = true;
                btnGuar.Visible = true;
            }
            else
            {
                txtCantidadRecomendado.Visible = false;
                lblCantidadRecomendado.Visible = false;
                lblDescuentoTotal.Visible = false;
                txtDescuento.Visible = false;
                btnGuar.Visible = false;
            }

        }

        private void txtOperacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void txtMonto_Validating(object sender, CancelEventArgs e)
        {
            if (txtMonto.Text != "" && txtMonto.Text != "160 Dolares")
            {
                txtMonto.Text = Convert.ToDecimal(txtMonto.Text).ToString("###,###.00");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

        }
        public void ActualizarDatosPago()
        {
            HabilitarTodosCampos();
            if (txtEstadoPago.Text == "No realizó ningún pago")
            {
                DataTable DatosPago = new DataTable();
                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                if (DatosPago.Rows.Count > 0)
                {
                    dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                    montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                    cmbNumeroCuota.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("No sé configuraron las cuotas de éste programa", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DeshabilitarTodosCampos();
                }

            }
            else if (txtEstadoPago.Text == "Falta cancelar Primera cuota")
            {
                DataTable DatosPago = new DataTable();
                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                if (DatosPago.Rows.Count > 0)
                {
                    dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                    montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                    cmbNumeroCuota.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("No sé configuraron las cuotas de éste programa", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DeshabilitarTodosCampos();
                }
            }
            else if (txtEstadoPago.Text == "Falta cancelar Segunda cuota")
            {
                DataTable DatosPago = new DataTable();
                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Segunda");
                if (DatosPago.Rows.Count > 0)
                {
                    dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                    montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                    cmbNumeroCuota.SelectedIndex = 1;
                }
                else
                {
                    MessageBox.Show("No sé configuraron las cuotas de éste programa", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DeshabilitarTodosCampos();
                }
            }
            else if (txtEstadoPago.Text == "Falta cancelar Tercera cuota")
            {
                DataTable DatosPago = new DataTable();
                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Tercera");
                if (DatosPago.Rows.Count > 0)
                {
                    dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                    montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                    cmbNumeroCuota.SelectedIndex = 2;
                }
                else
                {
                    MessageBox.Show("No sé configuraron las cuotas de éste programa", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DeshabilitarTodosCampos();
                }
            }
            else if (txtEstadoPago.Text == "Falta cancelar Comportamiento-Charla-Vuelo y Sevis")
            {
                DataTable DatosPago = new DataTable();
                DatosPago = pagoNegocio.BuscarDatosPago("1", "Comportamiento-Charla-Vuelo y Sevis");
                if (DatosPago.Rows.Count > 0)
                {
                    dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                    montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                    cmbNumeroCuota.SelectedIndex = 3;
                }
                else
                {
                    MessageBox.Show("No sé configuraron las cuotas de éste programa", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DeshabilitarTodosCampos();
                }
            }
            else if (txtEstadoPago.Text == "Cancelado")
            {
                MessageBox.Show("Participante ya canceló todo el programa :D", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DeshabilitarTodosCampos();
            }

        }
        public void LimpiarCampos()
        {
            if (cmbNumeroCuota.SelectedIndex != 3)
            {
                txtMonto.Text = "";
            }
            txtOperacion.Text = "";
            dtpFechaPago.Value = DateTime.Today;
        }

        private void cmbNumeroCuota_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbNumeroCuota.SelectedIndex == 3)
            {
                txtMonto.Text = "160 Dolares";
                txtMonto.ReadOnly = true;

            }
            else
            {
                txtMonto.Text = "";
                txtMonto.ReadOnly = false;
            }
        }
        private void DeshabilitarTodosCampos()
        {
            txtMonto.Enabled = false;
            txtOperacion.Enabled = false;
            btnGuardar.Enabled = false;
        }
        private void HabilitarTodosCampos()
        {
            txtMonto.Enabled = true;
            txtOperacion.Enabled = true;
            btnGuardar.Enabled = true;

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void txtMonto_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:

                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    btnBuscar.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void cmbNumeroCuota_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void dtpFechaPago_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:

                    SendKeys.Send("{TAB}");
                    if (dtpFechaPago.Value.Date.AddDays(1) != DateTime.Today)

                    {
                        dtpFechaPago.MaxDate = DateTime.Today.AddDays(1);
                        dtpFechaPago.Value = dtpFechaPago.Value.AddDays(1);

                    }

                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtMonto.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void cmbOperTrans_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    if (cmbOperTrans.SelectedIndex != -1)
                    {
                        cmbOperTrans.SelectedIndex = cmbOperTrans.SelectedIndex - 1;
                    }

                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:

                    dtpFechaPago.Focus();
                    if (cmbOperTrans.SelectedIndex == 0)
                    {
                        cmbOperTrans.SelectedIndex = cmbOperTrans.SelectedIndex - 1;
                    }
                    else if (cmbOperTrans.SelectedIndex == 1)

                    {
                        cmbOperTrans.SelectedIndex = cmbOperTrans.SelectedIndex - 2;
                    }
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtOperacion_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    cmbOperTrans.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }


        private void dtpFechaPago_Validated(object sender, EventArgs e)
        {
            dtpFechaPago.MaxDate = DateTime.Today;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (cmbOperTrans.SelectedIndex == 0)
            {
                pago.numeroVoucher = "OP-" + txtOperacion.Text;
            }
            else
            {
                pago.numeroVoucher = "TR-" + txtOperacion.Text;
            }
            if (entrevistaNegocio.VerificarVoucher(pago.numeroVoucher).Rows.Count > 0)
            {
                MessageBox.Show("El número de operación ya existe", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (txtMonto.Text == "" || cmbOperTrans.SelectedIndex == -1 || txtOperacion.Text == "")
            {
                MessageBox.Show("Rellenen los datos completos", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                if (cmbOperTrans.SelectedIndex == 0)
                {
                    pago.numeroVoucher = "OP-" + txtOperacion.Text;
                }
                else
                {
                    pago.numeroVoucher = "TR-" + txtOperacion.Text;
                }
                DialogResult dialogResult = MessageBox.Show("Los datos a guardar son los siguientes: \n \n MONTO: " + txtMonto.Text + "\n FECHA DE PAGO: " + dtpFechaPago.Value.ToString("dd/MM/yyyy") + "\n NÚMERO DE VOUCHER: " + pago.numeroVoucher + "\n \n Estás seguro que deseas guardar ?", "ADVERTENCIA", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (cmbNumeroCuota.SelectedIndex == 3)
                    {
                        pago.montoPago = 160;
                    }
                    else
                    {
                        pago.montoPago = Convert.ToDecimal(txtMonto.Text);
                    }

                    pago.numeroCuota = cmbNumeroCuota.Text;
                    pago.fechaPago = dtpFechaPago.Value;
                    pago.idPostulante = idParticipante;
                    pago.Encargado = idEncargado;
                    if (cmbOperTrans.SelectedIndex == 0)
                    {
                        pago.numeroVoucher = "OP-" + txtOperacion.Text;
                    }
                    else
                    {
                        pago.numeroVoucher = "TR-" + txtOperacion.Text;
                    }

                    bool correcto;
                    correcto = pagoNegocio.Insertar(pago, DateTime.Now);
                    if (correcto)
                    {
                        MessageBox.Show("Datos Guardados");
                        dgvPagos.DataSource = pagoNegocio.Buscar(idParticipante);
                        decimal sum = 0;
                        //Suma de Cuotas
                        if (dgvPagos.Rows.Count > 0)
                        {
                            for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                            {
                                if (dgvPagos.Rows[i].Cells[0].Value.ToString() == cmbNumeroCuota.Text)
                                {
                                    sum += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                                }

                            }

                        }
                        else
                        {
                            sum = 0;
                        }
                        decimal sumaDeuda = 0;
                        txtDeudaPend.Text = txtMontoPrograma.Text;
                        for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                        {
                            if (dgvPagos.Rows[i].Cells[0].Value.ToString() != "Comportamiento-Charla-Vuelo y Sevis")
                            {
                                sumaDeuda += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                                txtDeudaPend.Text = (Convert.ToDecimal(txtMontoPrograma.Text) - sumaDeuda).ToString();
                            }
                        }
                        if (Convert.ToDecimal(txtDeudaPend.Text) <= 0 && txtEstadoPago.Text != "Cancelado" && txtEstadoPago.Text != "Falta cancelar Comportamiento-Charla-Vuelo y Sevis")
                        {
                            pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                        }
                        else
                        {

                            if (sum < montoCuota)
                            {
                                if (cmbNumeroCuota.Text == "Primera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Primera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Segunda")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Segunda cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Tercera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Tercera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                                }
                            }

                            else if (sum == montoCuota)
                            {


                                if (cmbNumeroCuota.Text == "Primera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Segunda cuota");

                                }
                                else if (cmbNumeroCuota.Text == "Segunda")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Tercera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Tercera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                                }
                                else if (cmbNumeroCuota.Text == "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Cancelado");
                                }

                            }
                            else
                            {
                                if (cmbNumeroCuota.Text == "Primera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Segunda cuota");

                                }
                                else if (cmbNumeroCuota.Text == "Segunda")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Tercera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Tercera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                                }
                                else if (cmbNumeroCuota.Text == "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Cancelado");
                                }
                            }
                        }
                        DataTable dt = new DataTable();
                        dt = pagoNegocio.BuscarEstadoPago(idParticipante);
                        txtEstadoPago.Text = dt.Rows[0][0].ToString();
                        if (dtFechaLimite.Value < dtpFechaPago.Value && sum < montoCuota)
                        {


                            MessageBox.Show("Está pagando fuera fecha, ser más puntual, y aún no termina de cancelar ésta cuota", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        }
                        else if (dtFechaLimite.Value < dtpFechaPago.Value)

                        {
                            MessageBox.Show("Está pagando fuera fecha, ser más puntual", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else if (dtFechaLimite.Value == dtpFechaPago.Value)
                        {
                            MessageBox.Show("Está pagando a las justas XD :C", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        ActualizarDatosPago();
                        LimpiarCampos();



                    }

                    else
                    {
                        MessageBox.Show("Ocurrió un error al grabar los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }



        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvPagos.Rows.Count == 0)
            {
                MessageBox.Show("No existen registros a eliminar", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (dgvPagos.Rows[dgvPagos.CurrentCell.RowIndex].Cells[1].Value.ToString() != dgvPagos.Rows[dgvPagos.Rows.Count - 1].Cells[1].Value.ToString())
            {
                MessageBox.Show("Solo se puede eliminar el ultimo registro", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {


                if (Convert.ToDateTime(dgvPagos.Rows[dgvPagos.CurrentCell.RowIndex].Cells[4].Value.ToString().Substring(0, 10)) == DateTime.Today)
                {
                    DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el pago?", "ADVERTENCIA", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        bool RegistroDescuento = false;
                        for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                        {
                            if (dgvPagos.Rows[i].Cells[0].Value.ToString() == "Descuento por Recomendación")
                            {
                                RegistroDescuento = true;
                            }
                        }

                        bool correcto;
                        if (RegistroDescuento)
                        {
                            correcto = pagoNegocio.EliminarRecomendados(idParticipante);
                        }
                        else
                        {
                            correcto = pagoNegocio.Eliminar(dgvPagos.Rows[dgvPagos.CurrentCell.RowIndex].Cells[1].Value.ToString());
                        }
                        
                        if (correcto)
                        {
                            string cuotaEliminada;
                            cuotaEliminada = dgvPagos.Rows[dgvPagos.CurrentCell.RowIndex].Cells[0].Value.ToString();
                            MessageBox.Show("Se elimino el pago", "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dgvPagos.DataSource = pagoNegocio.Buscar(idParticipante);

                            decimal sum = 0;
                            //Suma de Cuotas
                            if (dgvPagos.Rows.Count > 0)
                            {
                                for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                                {
                                    if (dgvPagos.Rows[i].Cells[0].Value.ToString() == dgvPagos.Rows[dgvPagos.Rows.Count - 1].Cells[0].Value.ToString())
                                    {
                                        sum += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                                    }

                                }

                            }
                            else
                            {
                                sum = 0;
                            }
                            decimal sumaDeuda = 0;
                            txtDeudaPend.Text = txtMontoPrograma.Text;
                            for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                            {
                                if (dgvPagos.Rows[i].Cells[0].Value.ToString() != "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    sumaDeuda += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                                    txtDeudaPend.Text = (Convert.ToDecimal(txtMontoPrograma.Text) - sumaDeuda).ToString();
                                }
                            }

                            if (dgvPagos.Rows.Count == 0)
                            {
                                pagoNegocio.ActualizarEstadopago(idParticipante, "No realizó ningún pago");
                            }
                            else if (dgvPagos.Rows[dgvPagos.Rows.Count - 1].Cells[0].Value.ToString() == "Primera")
                            {

                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                                dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                                cmbNumeroCuota.SelectedIndex = 0;
                            }
                            else if (dgvPagos.Rows[dgvPagos.Rows.Count - 1].Cells[0].Value.ToString() == "Segunda")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Segunda");
                                dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                                cmbNumeroCuota.SelectedIndex = 1;
                            }
                            else if (dgvPagos.Rows[dgvPagos.Rows.Count - 1].Cells[0].Value.ToString() == "Tercera")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Tercera");
                                dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                                cmbNumeroCuota.SelectedIndex = 2;
                            }
                            else if (dgvPagos.Rows[dgvPagos.Rows.Count - 1].Cells[0].Value.ToString() == "Comportamiento-Charla-Vuelo y Sevis")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago("1", "Comportamiento-Charla-Vuelo y Sevis");
                                dtFechaLimite.Value = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                montoCuota = Convert.ToDecimal(DatosPago.Rows[0][1].ToString());
                                cmbNumeroCuota.SelectedIndex = 3;
                            }


                            if (sum < montoCuota)
                            {
                                if (cmbNumeroCuota.Text == "Primera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Primera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Segunda")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Segunda cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Tercera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Tercera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                                }
                            }

                            else if (sum == montoCuota)
                            {


                                if (cmbNumeroCuota.Text == "Primera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Segunda cuota");

                                }
                                else if (cmbNumeroCuota.Text == "Segunda")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Tercera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Tercera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                                }
                                else if (cmbNumeroCuota.Text == "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Cancelado");
                                }

                            }
                            else
                            {
                                if (cmbNumeroCuota.Text == "Primera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Segunda cuota");

                                }
                                else if (cmbNumeroCuota.Text == "Segunda")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Tercera cuota");
                                }
                                else if (cmbNumeroCuota.Text == "Tercera")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                                }
                                else if (cmbNumeroCuota.Text == "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    pagoNegocio.ActualizarEstadopago(idParticipante, "Cancelado");
                                }
                            }


                            if (txtDeudaPend.Text == "0")
                            {
                                pagoNegocio.ActualizarEstadopago(idParticipante, "Falta cancelar Comportamiento-Charla-Vuelo y Sevis");
                            }
                            if (dgvPagos.Rows.Count == 0)
                            {
                                pagoNegocio.ActualizarEstadopago(idParticipante, "No realizó ningún pago");
                            }
                            DataTable dt = new DataTable();
                            dt = pagoNegocio.BuscarEstadoPago(idParticipante);
                            txtEstadoPago.Text = dt.Rows[0][0].ToString();


                            txtMontoPrograma.Text = dt.Rows[0][2].ToString();
                            txtMoneda.Text = dt.Rows[0][4].ToString();
                            idPrograma = dt.Rows[0][1].ToString();

                            ActualizarDatosPago();
                            LimpiarCampos();
                            txtDeudaPend.Text = txtMontoPrograma.Text;
                            sumaDeuda = 0;
                            for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                            {
                                if (dgvPagos.Rows[i].Cells[0].Value.ToString() != "Comportamiento-Charla-Vuelo y Sevis")
                                {
                                    sumaDeuda += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                                    txtDeudaPend.Text = (Convert.ToDecimal(txtMontoPrograma.Text) - sumaDeuda).ToString();
                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("Ocurrio un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Solo se puede eliminar registros de hoy", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txtCantidadRecomendado_ValueChanged(object sender, EventArgs e)
        {
            if (txtMoneda.Text == "(Dólares)")
            {
                txtDescuento.Text = (txtCantidadRecomendado.Value * 30).ToString();
            }
        }

        private void btnGuar_Click(object sender, EventArgs e)
        {
            if (txtDescuento.Text == "0")
            {
                MessageBox.Show("No se puede registrar si tiene 0 recomendados.", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                bool RegistroDescuento = false;
                for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                {
                    if (dgvPagos.Rows[i].Cells[0].Value.ToString() == "Descuento por Recomendación")
                    {
                        RegistroDescuento = true;
                    }
                }
                
                pago.montoPago = Convert.ToDecimal(txtDescuento.Text);

                pago.numeroCuota = "Descuento por Recomendación";
                pago.fechaPago = DateTime.Today;
                pago.idPostulante = idParticipante;
                pago.Encargado = idEncargado;

                pago.numeroVoucher = "";

                bool correcto;

                if (RegistroDescuento)
                {
                    correcto = pagoNegocio.ModificarRecomendados(Convert.ToDecimal(txtDescuento.Text), idParticipante);
                }
                else
                {
                    correcto = pagoNegocio.Insertar(pago, DateTime.Now);
                }
                if (correcto)
                {
                    MessageBox.Show("Datos Guardados");
                    dgvPagos.DataSource = pagoNegocio.Buscar(idParticipante);
                    decimal sum = 0;
                    //Suma de Cuotas
                    if (dgvPagos.Rows.Count > 0)
                    {
                        for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                        {
                            if (dgvPagos.Rows[i].Cells[0].Value.ToString() == cmbNumeroCuota.Text)
                            {
                                sum += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                            }

                        }

                    }
                    else
                    {
                        sum = 0;
                    }
                    decimal sumaDeuda = 0;
                    txtDeudaPend.Text = txtMontoPrograma.Text;
                    for (int i = 0; i < dgvPagos.Rows.Count; ++i)
                    {
                        if (dgvPagos.Rows[i].Cells[0].Value.ToString() != "Comportamiento-Charla-Vuelo y Sevis")
                        {
                            sumaDeuda += Convert.ToDecimal(dgvPagos.Rows[i].Cells[3].Value);
                            txtDeudaPend.Text = (Convert.ToDecimal(txtMontoPrograma.Text) - sumaDeuda).ToString();
                        }
                    }

                }
                

                if ((txtEstadoPago.Text == "Falta cancelar Tercera cuota" || txtEstadoPago.Text == "Falta cancelar Comportamiento-Charla-Vuelo y Sevis") )
                {
                    txtCantidadRecomendado.Visible = true;
                    lblCantidadRecomendado.Visible = true;
                    lblDescuentoTotal.Visible = true;
                    txtDescuento.Visible = true;
                    btnGuar.Visible = true;
                }
                else
                {
                    txtCantidadRecomendado.Visible = false;
                    lblCantidadRecomendado.Visible = false;
                    lblDescuentoTotal.Visible = false;
                    txtDescuento.Visible = false;
                    btnGuar.Visible = false;
                }
            }

        }
    }
}
