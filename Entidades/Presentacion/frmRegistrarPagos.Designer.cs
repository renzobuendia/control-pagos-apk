﻿namespace Presentacion
{
    partial class frmRegistrarPagos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistrarPagos));
            this.dgvPagos = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtFechaLimite = new System.Windows.Forms.DateTimePicker();
            this.cmbNumeroCuota = new System.Windows.Forms.ComboBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCantidadRecomendado = new System.Windows.Forms.NumericUpDown();
            this.lblCantidadRecomendado = new System.Windows.Forms.Label();
            this.txtEstadoPago = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMontoPrograma = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMoneda = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbOperTrans = new System.Windows.Forms.ComboBox();
            this.txtOperacion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.dtpFechaPago = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDeudaPend = new System.Windows.Forms.TextBox();
            this.btnGuar = new System.Windows.Forms.Button();
            this.txtDescuento = new System.Windows.Forms.TextBox();
            this.lblDescuentoTotal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCantidadRecomendado)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPagos
            // 
            this.dgvPagos.AllowUserToAddRows = false;
            this.dgvPagos.AllowUserToResizeColumns = false;
            this.dgvPagos.AllowUserToResizeRows = false;
            this.dgvPagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPagos.Location = new System.Drawing.Point(16, 158);
            this.dgvPagos.MultiSelect = false;
            this.dgvPagos.Name = "dgvPagos";
            this.dgvPagos.ReadOnly = true;
            this.dgvPagos.RowHeadersVisible = false;
            this.dgvPagos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPagos.Size = new System.Drawing.Size(422, 169);
            this.dgvPagos.TabIndex = 25;
            this.dgvPagos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Fecha Límite:";
            // 
            // txtMonto
            // 
            this.txtMonto.Location = new System.Drawing.Point(195, 359);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(100, 20);
            this.txtMonto.TabIndex = 1;
            this.txtMonto.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.txtMonto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMonto_KeyDown);
            this.txtMonto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            this.txtMonto.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonto_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 366);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Monto:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 393);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Número de Cuota:";
            // 
            // dtFechaLimite
            // 
            this.dtFechaLimite.Enabled = false;
            this.dtFechaLimite.Location = new System.Drawing.Point(195, 333);
            this.dtFechaLimite.Name = "dtFechaLimite";
            this.dtFechaLimite.Size = new System.Drawing.Size(200, 20);
            this.dtFechaLimite.TabIndex = 3;
            // 
            // cmbNumeroCuota
            // 
            this.cmbNumeroCuota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNumeroCuota.Enabled = false;
            this.cmbNumeroCuota.FormattingEnabled = true;
            this.cmbNumeroCuota.Items.AddRange(new object[] {
            "Primera",
            "Segunda",
            "Tercera",
            "Comportamiento-Charla-Vuelo y Sevis"});
            this.cmbNumeroCuota.Location = new System.Drawing.Point(195, 385);
            this.cmbNumeroCuota.Name = "cmbNumeroCuota";
            this.cmbNumeroCuota.Size = new System.Drawing.Size(200, 21);
            this.cmbNumeroCuota.TabIndex = 4;
            this.cmbNumeroCuota.SelectedIndexChanged += new System.EventHandler(this.cmbNumeroCuota_SelectedIndexChanged);
            this.cmbNumeroCuota.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbNumeroCuota_KeyDown);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(358, 15);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(80, 30);
            this.btnBuscar.TabIndex = 0;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(82, 21);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(270, 20);
            this.txtNombre.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Postulante";
            // 
            // txtCantidadRecomendado
            // 
            this.txtCantidadRecomendado.Location = new System.Drawing.Point(195, 465);
            this.txtCantidadRecomendado.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtCantidadRecomendado.Name = "txtCantidadRecomendado";
            this.txtCantidadRecomendado.Size = new System.Drawing.Size(68, 20);
            this.txtCantidadRecomendado.TabIndex = 5;
            this.txtCantidadRecomendado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCantidadRecomendado.Visible = false;
            this.txtCantidadRecomendado.ValueChanged += new System.EventHandler(this.txtCantidadRecomendado_ValueChanged);
            // 
            // lblCantidadRecomendado
            // 
            this.lblCantidadRecomendado.AutoSize = true;
            this.lblCantidadRecomendado.Location = new System.Drawing.Point(43, 472);
            this.lblCantidadRecomendado.Name = "lblCantidadRecomendado";
            this.lblCantidadRecomendado.Size = new System.Drawing.Size(148, 13);
            this.lblCantidadRecomendado.TabIndex = 2;
            this.lblCantidadRecomendado.Text = "Cantidad de Recomendados: ";
            this.lblCantidadRecomendado.Visible = false;
            // 
            // txtEstadoPago
            // 
            this.txtEstadoPago.Enabled = false;
            this.txtEstadoPago.Location = new System.Drawing.Point(16, 82);
            this.txtEstadoPago.Name = "txtEstadoPago";
            this.txtEstadoPago.ReadOnly = true;
            this.txtEstadoPago.Size = new System.Drawing.Size(221, 20);
            this.txtEstadoPago.TabIndex = 18;
            this.txtEstadoPago.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Estado de Pago:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtMontoPrograma
            // 
            this.txtMontoPrograma.Enabled = false;
            this.txtMontoPrograma.Location = new System.Drawing.Point(104, 131);
            this.txtMontoPrograma.Name = "txtMontoPrograma";
            this.txtMontoPrograma.ReadOnly = true;
            this.txtMontoPrograma.Size = new System.Drawing.Size(125, 20);
            this.txtMontoPrograma.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(101, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Monto total del Programa:";
            // 
            // txtMoneda
            // 
            this.txtMoneda.Enabled = false;
            this.txtMoneda.Location = new System.Drawing.Point(254, 131);
            this.txtMoneda.Name = "txtMoneda";
            this.txtMoneda.ReadOnly = true;
            this.txtMoneda.Size = new System.Drawing.Size(87, 20);
            this.txtMoneda.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(251, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Moneda:";
            // 
            // cmbOperTrans
            // 
            this.cmbOperTrans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOperTrans.FormattingEnabled = true;
            this.cmbOperTrans.Items.AddRange(new object[] {
            "Operación",
            "Transferencia"});
            this.cmbOperTrans.Location = new System.Drawing.Point(195, 438);
            this.cmbOperTrans.Name = "cmbOperTrans";
            this.cmbOperTrans.Size = new System.Drawing.Size(93, 21);
            this.cmbOperTrans.TabIndex = 3;
            this.cmbOperTrans.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOperTrans_KeyDown);
            // 
            // txtOperacion
            // 
            this.txtOperacion.Location = new System.Drawing.Point(294, 438);
            this.txtOperacion.Name = "txtOperacion";
            this.txtOperacion.Size = new System.Drawing.Size(101, 20);
            this.txtOperacion.TabIndex = 4;
            this.txtOperacion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOperacion_KeyDown);
            this.txtOperacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperacion_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 441);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Numero de:";
            // 
            // btnSalir
            // 
            this.btnSalir.Image = global::Presentacion.Properties.Resources.icons8_close_program_48;
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.Location = new System.Drawing.Point(277, 548);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(65, 69);
            this.btnSalir.TabIndex = 9;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::Presentacion.Properties.Resources.icons8_save_48;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGuardar.Location = new System.Drawing.Point(80, 548);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(65, 69);
            this.btnGuardar.TabIndex = 8;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = global::Presentacion.Properties.Resources.icons8_trash_48;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEliminar.Location = new System.Drawing.Point(182, 548);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(65, 69);
            this.btnEliminar.TabIndex = 7;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // dtpFechaPago
            // 
            this.dtpFechaPago.Location = new System.Drawing.Point(195, 412);
            this.dtpFechaPago.Name = "dtpFechaPago";
            this.dtpFechaPago.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaPago.TabIndex = 2;
            this.dtpFechaPago.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFechaPago_KeyDown);
            this.dtpFechaPago.Validated += new System.EventHandler(this.dtpFechaPago_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(43, 419);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Fecha de pago:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(267, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Deuda Pendiente:";
            // 
            // txtDeudaPend
            // 
            this.txtDeudaPend.Enabled = false;
            this.txtDeudaPend.Location = new System.Drawing.Point(270, 82);
            this.txtDeudaPend.Name = "txtDeudaPend";
            this.txtDeudaPend.ReadOnly = true;
            this.txtDeudaPend.Size = new System.Drawing.Size(125, 20);
            this.txtDeudaPend.TabIndex = 20;
            // 
            // btnGuar
            // 
            this.btnGuar.Location = new System.Drawing.Point(277, 466);
            this.btnGuar.Name = "btnGuar";
            this.btnGuar.Size = new System.Drawing.Size(75, 19);
            this.btnGuar.TabIndex = 48;
            this.btnGuar.Text = "GUARDAR";
            this.btnGuar.UseVisualStyleBackColor = true;
            this.btnGuar.Visible = false;
            this.btnGuar.Click += new System.EventHandler(this.btnGuar_Click);
            // 
            // txtDescuento
            // 
            this.txtDescuento.Location = new System.Drawing.Point(195, 492);
            this.txtDescuento.Name = "txtDescuento";
            this.txtDescuento.ReadOnly = true;
            this.txtDescuento.Size = new System.Drawing.Size(100, 20);
            this.txtDescuento.TabIndex = 49;
            this.txtDescuento.Text = "0";
            this.txtDescuento.Visible = false;
            // 
            // lblDescuentoTotal
            // 
            this.lblDescuentoTotal.AutoSize = true;
            this.lblDescuentoTotal.Location = new System.Drawing.Point(43, 499);
            this.lblDescuentoTotal.Name = "lblDescuentoTotal";
            this.lblDescuentoTotal.Size = new System.Drawing.Size(89, 13);
            this.lblDescuentoTotal.TabIndex = 50;
            this.lblDescuentoTotal.Text = "Descuento Total:";
            this.lblDescuentoTotal.Visible = false;
            // 
            // frmRegistrarPagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 629);
            this.Controls.Add(this.txtDescuento);
            this.Controls.Add(this.lblDescuentoTotal);
            this.Controls.Add(this.btnGuar);
            this.Controls.Add(this.txtDeudaPend);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dtpFechaPago);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.cmbOperTrans);
            this.Controls.Add(this.txtOperacion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMoneda);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtMontoPrograma);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtEstadoPago);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblCantidadRecomendado);
            this.Controls.Add(this.txtCantidadRecomendado);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbNumeroCuota);
            this.Controls.Add(this.dtFechaLimite);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMonto);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvPagos);
            this.Name = "frmRegistrarPagos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar pagos";
            this.Load += new System.EventHandler(this.frmCuota_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCantidadRecomendado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvPagos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtFechaLimite;
        private System.Windows.Forms.ComboBox cmbNumeroCuota;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown txtCantidadRecomendado;
        private System.Windows.Forms.Label lblCantidadRecomendado;
        private System.Windows.Forms.TextBox txtEstadoPago;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMontoPrograma;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMoneda;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbOperTrans;
        private System.Windows.Forms.TextBox txtOperacion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.DateTimePicker dtpFechaPago;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDeudaPend;
        private System.Windows.Forms.Button btnGuar;
        private System.Windows.Forms.TextBox txtDescuento;
        private System.Windows.Forms.Label lblDescuentoTotal;
    }
}