﻿namespace Presentacion
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuPostulantes = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEntrevista = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuPagos = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuCuotas = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPORTEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuPostulantes,
            this.MenuEntrevista,
            this.MenuPagos,
            this.MenuCuotas,
            this.rEPORTEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1286, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MenuPostulantes
            // 
            this.MenuPostulantes.Name = "MenuPostulantes";
            this.MenuPostulantes.Size = new System.Drawing.Size(98, 20);
            this.MenuPostulantes.Text = "POSTULANTES";
            this.MenuPostulantes.Click += new System.EventHandler(this.MenuPostulantes_Click);
            // 
            // MenuEntrevista
            // 
            this.MenuEntrevista.Name = "MenuEntrevista";
            this.MenuEntrevista.Size = new System.Drawing.Size(84, 20);
            this.MenuEntrevista.Text = "ENTREVISTA";
            this.MenuEntrevista.Click += new System.EventHandler(this.MenuEntrevista_Click);
            // 
            // MenuPagos
            // 
            this.MenuPagos.Name = "MenuPagos";
            this.MenuPagos.Size = new System.Drawing.Size(56, 20);
            this.MenuPagos.Text = "PAGOS";
            this.MenuPagos.Click += new System.EventHandler(this.MenuCuotas_Click);
            // 
            // MenuCuotas
            // 
            this.MenuCuotas.Name = "MenuCuotas";
            this.MenuCuotas.Size = new System.Drawing.Size(63, 20);
            this.MenuCuotas.Text = "CUOTAS";
            this.MenuCuotas.Click += new System.EventHandler(this.cUOTASToolStripMenuItem_Click);
            // 
            // rEPORTEToolStripMenuItem
            // 
            this.rEPORTEToolStripMenuItem.Name = "rEPORTEToolStripMenuItem";
            this.rEPORTEToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.rEPORTEToolStripMenuItem.Text = "REPORTE";
            this.rEPORTEToolStripMenuItem.Click += new System.EventHandler(this.rEPORTEToolStripMenuItem_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1286, 632);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de Pagos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuPostulantes;
        private System.Windows.Forms.ToolStripMenuItem MenuEntrevista;
        private System.Windows.Forms.ToolStripMenuItem MenuPagos;
        public System.Windows.Forms.ToolStripMenuItem MenuCuotas;
        private System.Windows.Forms.ToolStripMenuItem rEPORTEToolStripMenuItem;
    }
}