﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Presentacion
{
    public partial class frmInicioSesion : Form
    {
        PagoNegocio pagoNegocio = new PagoNegocio();
        Entidades.Usuario usuario = new Entidades.Usuario();
        LogicaNegocio.LoginNegocio loginNegocio = new LogicaNegocio.LoginNegocio();

        string EstadoPago;
        DateTime FechaLimite;
        string idPrograma = "";
        public frmInicioSesion()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataTable nomusuario = new DataTable();
            usuario.usuario = txtUsuario.Text;
            usuario.password = TxtContraseña.Text;
            dt = loginNegocio.Login(usuario);
            nomusuario = loginNegocio.ComprobarUsuario(usuario);

            if (nomusuario.Rows.Count > 0)
            {

                if (dt.Rows.Count > 0)
                {
                    var frmPrincipal = new frmPrincipal();
                    frmPrincipal.Closed += (s, args) => this.Close();
                    frmPrincipal.Show();
                    usuario.usuario = dt.Rows[0][1].ToString();
                    frmPrincipal.usuario = usuario.usuario;
                    frmPrincipal.idUsuario = dt.Rows[0][0].ToString();
                    this.Hide();
                    if (frmPrincipal.usuario == "Mishell")
                    {
                        
                        // read connectionstring from config file
                        var connectionString = ConfigurationManager.ConnectionStrings["sql"].ConnectionString;

                        // read backup folder from config file ("C:/temp/")
                        var backupFolder = ConfigurationManager.AppSettings["BackupFolder"];

                        var sqlConStrBuilder = new SqlConnectionStringBuilder(connectionString);

                        
                        // set backupfilename (you will get something like: "C:/temp/MyDatabase-2013-12-07.bak")
                        var backupFileName = String.Format("{0}{1}-{2}.bak",
                            backupFolder, sqlConStrBuilder.InitialCatalog,
                            DateTime.Now.ToString("yyyy-MM-dd"));
                        if (!File.Exists(backupFileName.ToString()))
                        {
                            using (var connection = new SqlConnection(sqlConStrBuilder.ConnectionString))
                            {
                                var query = String.Format("BACKUP DATABASE {0} TO DISK='{1}'",
                                    sqlConStrBuilder.InitialCatalog, backupFileName);

                                using (var command = new SqlCommand(query, connection))
                                {
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                }
                            }
                        } 
                    }
                    if (frmPrincipal.usuario == "Sonia" || frmPrincipal.usuario == "Miguel" || frmPrincipal.usuario == "Mishell")
                    {
                        frmPrincipal.MenuCuotas.Visible = true;
                    }
                    else
                    {
                        frmPrincipal.MenuCuotas.Visible = false;
                    }
                    MessageBox.Show("Bienvenid@: " + frmPrincipal.usuario);
                    DataTable ListaParticipantes = new DataTable();
                    ListaParticipantes = loginNegocio.DevolverListaParticipantes();
                    for (int i = 0; i < ListaParticipantes.Rows.Count; i++)
                    {
                        dt = pagoNegocio.BuscarEstadoPago(ListaParticipantes.Rows[i][0].ToString());
                        if (frmPrincipal.usuario == "Sonia" || frmPrincipal.usuario == "Miguel")
                        {
                            EstadoPago = dt.Rows[0][0].ToString();
                            idPrograma = dt.Rows[0][1].ToString();
                            if (EstadoPago == "No realizó ningún pago")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Primera cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }

                            }
                            else if (EstadoPago == "Falta cancelar Primera cuota")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Primera cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {


                                }
                            }
                            else if (EstadoPago == "Falta cancelar Segunda cuota")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Segunda");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Segunda Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Segunda Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Segunda cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }
                            }
                            else if (EstadoPago == "Falta cancelar Tercera cuota")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Tercera");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Tercera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Tercera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Tercera cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }
                            }
                            else if (EstadoPago == "Falta cancelar Comportamiento-Charla-Vuelo y Sevis")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago("1", "Comportamiento-Charla-Vuelo y Sevis");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Cuota de Comportamiento: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Cuota de Comportamiento: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Cuota de Comportamiento de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }
                            }
                        }

                        if (frmPrincipal.usuario == ListaParticipantes.Rows[i][3].ToString())

                        {
                            EstadoPago = dt.Rows[0][0].ToString();
                            idPrograma = dt.Rows[0][1].ToString();
                            if (EstadoPago == "No realizó ningún pago")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Primera cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }

                            }
                            else if (EstadoPago == "Falta cancelar Primera cuota")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Primera");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Primera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Primera cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {


                                }
                            }
                            else if (EstadoPago == "Falta cancelar Segunda cuota")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Segunda");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Segunda Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Segunda Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Segunda cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }
                            }
                            else if (EstadoPago == "Falta cancelar Tercera cuota")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago(idPrograma, "Tercera");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Tercera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Tercera Cuota: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Tercera cuota de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }
                            }
                            else if (EstadoPago == "Falta cancelar Comportamiento-Charla-Vuelo y Sevis")
                            {
                                DataTable DatosPago = new DataTable();
                                DatosPago = pagoNegocio.BuscarDatosPago("1", "Comportamiento-Charla-Vuelo y Sevis");
                                if (DatosPago.Rows.Count > 0)
                                {
                                    FechaLimite = Convert.ToDateTime(DatosPago.Rows[0][0].ToString());
                                    if (FechaLimite < DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante debe la Cuota de Comportamiento: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite == DateTime.Today)
                                    {
                                        MessageBox.Show("Éste participante tiene que pagar hoy la Cuota de Comportamiento: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                    else if (FechaLimite <= DateTime.Today.AddDays(3))
                                    {
                                        MessageBox.Show("Dentro de 3 días se vence la fecha de pago de la Cuota de Comportamiento de éste participantes: " + ListaParticipantes.Rows[i][1].ToString() + " " + ListaParticipantes.Rows[i][2].ToString());
                                    }
                                }
                                else
                                {

                                }
                            }
                        }

                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("La contraseña es incorrecta");
                    TxtContraseña.Text = "";
                    TxtContraseña.Focus();
                }

            }
            else
            {
                MessageBox.Show("El Usuario no existe");
                txtUsuario.Text = "";
                TxtContraseña.Text = "";
                txtUsuario.Focus();
            }

        }
    }
}
