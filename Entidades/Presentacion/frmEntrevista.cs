﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmEntrevista : Form
    {
        string idParticipante = "";
        string idEncargado;
        string encargado;

        Entidades.Entrevista entrevista = new Entidades.Entrevista();
        EntrevistaNegocio entrevistaNegocio = new EntrevistaNegocio();
        public frmEntrevista(string idEncargado, string encargado)
        {
            InitializeComponent();
            this.idEncargado = idEncargado;
            this.encargado = encargado;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            using (var form = new frmBuscar())
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Entidades.Participante participanteBusqueda = new Entidades.Participante();            //values preserved after close
                    participanteBusqueda = form.participante;

                    idParticipante = participanteBusqueda.id;
                    txtNombre.Text = participanteBusqueda.nombre + " " + participanteBusqueda.apellido;

                    DataTable dt = new DataTable();
                    dt = entrevistaNegocio.Buscar(idParticipante);


                    if (dt.Rows.Count > 0)
                    {

                        entrevista.idPostulante = idParticipante;
                        entrevista.encargado = dt.Rows[0][0].ToString();
                        entrevista.montoEntrevista = Convert.ToDecimal(dt.Rows[0][1].ToString());
                        entrevista.descripcion = dt.Rows[0][2].ToString();
                        entrevista.fechaEntrevista = Convert.ToDateTime(dt.Rows[0][3].ToString());
                        entrevista.fechaPago = Convert.ToDateTime(dt.Rows[0][6].ToString());
                        if (dt.Rows[0][4].ToString().Substring(0, 3) == "OP-")
                        {
                            cmbOperTrans.SelectedIndex = 0;
                        }
                        else
                        {
                            cmbOperTrans.SelectedIndex = 1;
                        }
                        entrevista.numeroOperacion = dt.Rows[0][4].ToString().Substring(3);

                        cboPrograma.SelectedValue = dt.Rows[0][5].ToString();
                        txtEncargado.Text = entrevista.encargado;
                        txtMonto.Text = entrevista.montoEntrevista.ToString();
                        txtDescripcion.Text = entrevista.descripcion;
                        dateEntrevista.Value = entrevista.fechaEntrevista;
                        dtPago.Value = entrevista.fechaPago;
                        txtOperacion.Text = entrevista.numeroOperacion;
                        lbInformacion.Text = "EL PARTICIPANTE YA TIENE ENTREVISTA";
                        lbInformacion.ForeColor = Color.Green;
                    }
                    else
                    {
                        cboPrograma.SelectedIndex = 0;
                        txtEncargado.Text = encargado;
                        txtMonto.Text = "";
                        txtDescripcion.Text = "";
                        dateEntrevista.Value = DateTime.Now;
                        dtPago.Value = DateTime.Today;
                        txtOperacion.Text = "";
                        lbInformacion.Text = "¡A ÉSTE PARTICIPANTE LE FALTA ENTREVISTA!";
                        lbInformacion.ForeColor = Color.Red;
                    }
                    txtMonto.Enabled = true;
                    txtDescripcion.Enabled = true;
                    dateEntrevista.Enabled = true;
                    dtPago.Enabled = true;
                    txtOperacion.Enabled = true;
                    btnGuardar.Enabled = true;
                }
            }
        }

        private void frmEntrevista_Load(object sender, EventArgs e)
        {
            txtEncargado.Text = encargado;

            cboPrograma.DataSource = entrevistaNegocio.BuscarPrograma();
            cboPrograma.DisplayMember = "Nombre";
            cboPrograma.ValueMember = "ID";
            dtPago.MaxDate = DateTime.Today;
        }

        private void txtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void txtMonto_Validating(object sender, CancelEventArgs e)
        {
            if (txtMonto.Text != "")
            {
                txtMonto.Text = Convert.ToDecimal(txtMonto.Text).ToString("###,###.00");
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtMonto.Text == "" || txtDescripcion.Text == "" || txtOperacion.Text == "" || cmbOperTrans.SelectedIndex < 0)
            {
                MessageBox.Show("Tiene que completar los campos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (idParticipante == "")
            {
                MessageBox.Show("Debe seleccionar un participante");
            }
            else
            {

                Entidades.Entrevista entrevista = new Entidades.Entrevista();
                DataTable dt = new DataTable();
                dt = entrevistaNegocio.Buscar(idParticipante);

                if (dt.Rows.Count > 0)
                {
                    entrevista.idPostulante = idParticipante;
                    entrevista.encargado = txtEncargado.Text;
                    entrevista.montoEntrevista = Convert.ToDecimal(txtMonto.Text);
                    entrevista.descripcion = txtDescripcion.Text;
                    entrevista.fechaEntrevista = dateEntrevista.Value;
                    entrevista.fechaPago = dtPago.Value;
                    if (cmbOperTrans.SelectedIndex == 0)
                    {
                        entrevista.numeroOperacion = "OP-" + txtOperacion.Text;
                    }
                    else
                    {
                        entrevista.numeroOperacion = "TR-" + txtOperacion.Text;
                    }

                    bool correcto;
                    correcto = entrevistaNegocio.Modificar(entrevista, cboPrograma.SelectedValue.ToString());
                    if (correcto)
                    {
                        MessageBox.Show("Datos Actualizados");
                    }

                    else if (entrevistaNegocio.VerificarVoucher(entrevista.numeroOperacion).Rows.Count > 0)
                    {
                        MessageBox.Show("El número de operación ya existe", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Ocurrió un error al grabar los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    entrevista.idPostulante = idParticipante;
                    entrevista.encargado = txtEncargado.Text;
                    entrevista.montoEntrevista = Convert.ToDecimal(txtMonto.Text);
                    entrevista.descripcion = txtDescripcion.Text;
                    entrevista.fechaEntrevista = dateEntrevista.Value;
                    entrevista.fechaPago = dtPago.Value;
                    if (cmbOperTrans.SelectedIndex == 0)
                    {
                        entrevista.numeroOperacion = "OP-" + txtOperacion.Text;
                    }
                    else
                    {
                        entrevista.numeroOperacion = "TR-" + txtOperacion.Text;
                    }

                    bool correcto;
                    correcto = entrevistaNegocio.Insertar(entrevista, cboPrograma.SelectedValue.ToString(), "No realizó ningún pago");
                    if (correcto)
                    {
                        MessageBox.Show("Datos Guardados");
                        lbInformacion.Text = "EL PARTICIPANTE YA TIENE ENTREVISTA";
                        lbInformacion.ForeColor = Color.Green;
                    }
                    else if (entrevistaNegocio.VerificarVoucher(entrevista.numeroOperacion).Rows.Count > 0)
                    {
                        MessageBox.Show("El número de operación ya existe", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Ocurrió un error al grabar los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtOperacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && !char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }


        }

        private void txtMonto_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboPrograma_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    cboPrograma.SelectedIndex = cboPrograma.SelectedIndex - 1;
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    btnBuscar.Focus();
                    if (cboPrograma.SelectedIndex == 0)
                    {
                        cboPrograma.SelectedIndex = cboPrograma.SelectedIndex - 1;
                    }
                    else if (cboPrograma.SelectedIndex >0 && cboPrograma.SelectedIndex < cboPrograma.Items.Count-1)

                    {
                        cboPrograma.SelectedIndex = cboPrograma.SelectedIndex +1 ;
                    }
                    else
                    {
                        cboPrograma.SelectedIndex = cboPrograma.SelectedIndex ;
                    }
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtEncargado_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    cboPrograma.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }

        }

        private void txtMonto_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    cboPrograma.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtDescripcion_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtMonto.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void dateEntrevista_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:

                    SendKeys.Send("{TAB}");
                    dateEntrevista.Value = dateEntrevista.Value.AddDays(1);
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtDescripcion.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void cmbOperTrans_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    if (cmbOperTrans.SelectedIndex != -1)
                    {
                        cmbOperTrans.SelectedIndex = cmbOperTrans.SelectedIndex - 1;
                    }

                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:

                    dateEntrevista.Focus();
                    if (cmbOperTrans.SelectedIndex == 0)
                    {
                        cmbOperTrans.SelectedIndex = cmbOperTrans.SelectedIndex - 1;
                    }
                    else
                    {
                        cmbOperTrans.SelectedIndex = cmbOperTrans.SelectedIndex - 2;
                    }
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void txtOperacion_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Up:
                    cmbOperTrans.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }



        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtPago_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dtPago_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    // SelectNext(this);
                    break;
                case Keys.Down:

                    SendKeys.Send("{TAB}");
                    if (dtPago.Value.Date.AddDays(1) != DateTime.Today)

                    {
                        dtPago.MaxDate = DateTime.Today.AddDays(1);
                        dtPago.Value = dtPago.Value.AddDays(1);

                    }

                    // SelectNext(this);
                    break;
                case Keys.Up:
                    txtMonto.Focus();
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        private void dtPago_Validated(object sender, EventArgs e)
        {
            dtPago.MaxDate = DateTime.Today;
        }
    }
}
