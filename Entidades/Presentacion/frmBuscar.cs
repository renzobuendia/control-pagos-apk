﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmBuscar : Form
    {
        LogicaNegocio.ParticipanteNegocio participanteNegocio = new LogicaNegocio.ParticipanteNegocio();
        public Entidades.Participante participante = new Entidades.Participante();
        public frmBuscar()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtvalor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dtgResultados.DataSource = participanteNegocio.Buscar(cbparametro.Text, txtvalor.Text);
                txtNumero.Text = dtgResultados.RowCount.ToString();
                if (dtgResultados.RowCount > 0)
                {
                    ActiveControl = dtgResultados;
                    btnAceptar.Enabled = true;
                }
                else
                {
                    btnAceptar.Enabled = false;
                }
            }
        }

        private void frmBuscar_Load(object sender, EventArgs e)
        {
            txtvalor.CharacterCasing = CharacterCasing.Upper;
            cbparametro.SelectedIndex = 0;
            ActiveControl = txtvalor;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (btnAceptar.Enabled)
            {
                participante.apellido = dtgResultados.Rows[dtgResultados.CurrentCell.RowIndex].Cells[2].Value.ToString();
                participante.nombre = dtgResultados.Rows[dtgResultados.CurrentCell.RowIndex].Cells[1].Value.ToString();
                participante.correo = dtgResultados.Rows[dtgResultados.CurrentCell.RowIndex].Cells[3].Value.ToString();
                participante.celular = dtgResultados.Rows[dtgResultados.CurrentCell.RowIndex].Cells[4].Value.ToString();
                participante.id = dtgResultados.Rows[dtgResultados.CurrentCell.RowIndex].Cells[0].Value.ToString();


                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void dtgResultados_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && dtgResultados.Rows.Count > 0)
            {

                button1_Click(this, new EventArgs());
            }
        }
    }
}
